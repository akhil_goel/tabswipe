package com.example.akhil.tabwithswipe;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class Tab2Fragment extends Fragment {
    TextView tab2;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        tab2 = (TextView)getView().findViewById(R.id.textOnTab2);
        return inflater.inflate(R.layout.tab2, container,false);
    }
    boolean loaded = false;
    public void setUserVisibleHint(boolean isVisibleToUser){
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser&& !loaded){
            loaded=true;
            loadData();
        }
    }
    public void loadData(){

        tab2.setText("one two three");

    }
}